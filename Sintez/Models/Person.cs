﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using MvvmLight = GalaSoft.MvvmLight;

namespace Sintez
{
    class Person : MvvmLight.ViewModelBase, ICloneable
    { 
        private string name;
        private string secondName;
        private string lastName;
        private string email;

        public ValidationErrors Errors { get; set; } = new ValidationErrors();


        [Required (ErrorMessage = "Необходимо ввести имя")]
        public string Name
        {
            get => name;
            set
            {
                name = value;
                RaisePropertyChanged();
            }
        }

        [Required (ErrorMessage = "Необходимо ввести отчество")]
        [StringLength(15, ErrorMessage = "Отчество должно быть не более 15 символов")]
        public string SecondName
        {
            get => secondName;
            set
            {
                secondName = value;
                RaisePropertyChanged();
            }
        }

        [Required(ErrorMessage = "Необходимо ввести фамилию")]
        [StringLength(15, MinimumLength = 5, ErrorMessage = "Фамилия должна быть от 5 до 15 символов")]
        public string LastName
        {
            get => lastName;
            set
            {
                lastName = value;
                RaisePropertyChanged();
            }
        }

        [Required]
        [EmailAddress]
        public string Email
        {
            get => email;
            set
            {
                email = value;
                RaisePropertyChanged();
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    /// <summary>
    /// Хранит текст ошибок валидации для каждого отдельно взятого поля для ввывода в соответствующие место
    /// </summary>
    class ValidationErrors : MvvmLight.ViewModelBase
    {
        private string nameError;
        private string secondNameError;
        private string lastNameError;
        private string emailError;


        public string NameError
        {
            get => nameError;
            set
            {
                nameError = value;
                RaisePropertyChanged();
            }
        }
        public string SecondNameError
        {
            get => secondNameError;
            set
            {
                secondNameError = value;
                RaisePropertyChanged();
            }
        }
        public string LastNameError
        {
            get => lastNameError;
            set
            {
                lastNameError = value;
                RaisePropertyChanged();
            }
        }
        public string EmailError
        {
            get => emailError;
            set
            {
                emailError = value;
                RaisePropertyChanged();
            }
        }
    }    
}
