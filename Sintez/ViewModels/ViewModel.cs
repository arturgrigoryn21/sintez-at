﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.IO;
using MvvmLight = GalaSoft.MvvmLight;
using System.Xml;

namespace Sintez
{
    class ViewModel : MvvmLight.ViewModelBase
    {
        #region Fields
        private Person selectedPerson;
        private ISerializer xmlSerializer;
        private ISerializer binarySerializer;
        private IDialogService dialogService;

        private ICommand refresh;
        private ICommand add;
        private ICommand remove;
        private ICommand openCommand;
        
        private ICommand saveXml;
        private ICommand saveBinary;
        #endregion

        #region Properties
        private ObservableCollection<Person> people = new ObservableCollection<Person>();
        public ObservableCollection<Person> People
        {
            get => people;
            set
            {
                people = value;
                RaisePropertyChanged();
            }
        }
        public Person SelectedPerson
        {
            get {return selectedPerson; }
            set
            {
                selectedPerson = value;
                Validation.Check(SelectedPerson);
                RaisePropertyChanged();
            }
        }
        public string Logs { get; set; }
       
        #endregion
        public ViewModel(IDialogService dialogService, ISerializer xmlSerializer, ISerializer binarySerializer)
        {
            this.dialogService = dialogService;
            this.xmlSerializer = xmlSerializer;
            this.binarySerializer = binarySerializer;

            SelectedPerson = new Person();
        }

        #region Commands
        public ICommand Refresh
        {
            get
            {
                return refresh ?? (refresh = new MvvmLight.Command.RelayCommand(() =>
                {
                    SelectedPerson = new Person();
                }));
            }
        }
        public ICommand Remove
        {
            get
            {
                return remove ?? (remove = new MvvmLight.Command.RelayCommand(() =>
                  {
                      if (People.Count > 0)
                          People.Remove(SelectedPerson);

                      if (People.Count > 0)
                          SelectedPerson = People[0];
                      else
                          SelectedPerson = new Person();
                  }));
            }
        }
        public ICommand Add
        {
            get
            {
                return add ?? (add = new MvvmLight.Command.RelayCommand(() =>
                {
                    if (Validation.Check(SelectedPerson))
                    {
                        SelectedPerson = SelectedPerson.Clone() as Person;
                        People.Insert(0, SelectedPerson);
                    }
                }));
            }
        }       
        public ICommand OpenCommand
        {
            get
            {
                return openCommand ??
                  (openCommand = new MvvmLight.Command.RelayCommand(() =>
                  {
                      try
                      {
                          if (dialogService.OpenFileDialog() == true)
                          {
                              if(dialogService.FilePath.Contains("xml"))
                                People = xmlSerializer.Deserialize(dialogService.FilePath) as ObservableCollection<Person>;
                              else if(dialogService.FilePath.Contains("dot"))
                                  People = binarySerializer.Deserialize(dialogService.FilePath) as ObservableCollection<Person>;
                              SelectedPerson = new Person();
                          }
                      }
                      //catch(XmlException)
                      //{
                      //    if (dialogService.OpenFileDialog() == true)
                      //    {
                      //        People = binarySerializer.Deserialize(dialogService.FilePath) as ObservableCollection<Person>;
                      //        SelectedPerson = new Person();
                      //    }
                      //}
                      catch(Exception ex)
                      {
                          dialogService.ShowMessage(ex.Message);
                      }

                  }));
            }
        }
        public ICommand SaveXml
        {
            get
            {
                return saveXml ??
                  (saveXml = new MvvmLight.Command.RelayCommand(() =>
                  {
                      try
                      {
                          if (dialogService.SaveFileDialog() == true)
                          {
                              if (Validation.Check(People, out string _logs))
                              {
                                  xmlSerializer.Serialize(People, dialogService.FilePath);
                              }
                              Logs = _logs ?? $"Файл xml сохранен!";
                              MessageBox.Show(Logs);
                          }
                      }
                      catch (Exception ex)
                      {
                          dialogService.ShowMessage(ex.Message);
                      }
                  }));
            }
        }
        public ICommand SaveBinary
        {
            get
            {
                return saveBinary ??
                  (saveBinary = new MvvmLight.Command.RelayCommand(() =>
                  {
                      try
                      {
                          if (dialogService.SaveFileDialog() == true)
                          {
                              if (Validation.Check(People, out string _logs))
                              {
                                  binarySerializer.Serialize(People, dialogService.FilePath);
                              }
                              Logs = _logs ?? $"Файл xml сохранен!";
                              MessageBox.Show(Logs);
                          }
                      }
                      catch (Exception ex)
                      {
                          dialogService.ShowMessage(ex.Message);
                      }
                  }));
            }
        }
        #endregion       
    }
}
