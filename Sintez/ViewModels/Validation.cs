﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using System.Windows;

namespace Sintez
{
    static class Validation
    {
        static public bool Check(Person person)
        {
            try
            {
                var results = new List<ValidationResult>();
                var context = new ValidationContext(person);
                if (!Validator.TryValidateObject(person, context, results, true))
                {
                    foreach (var error in results)
                        Notes(error, person);
                }
                else
                    CleanNotes(person);
                return results.Count == 0;
            }
            catch(ArgumentNullException)
            {
                return true;
            }
            
        }

        static public bool Check(IEnumerable<Person> people, out string _logs)
        {
            List<bool> values=new List<bool>();
            _logs = null;
            foreach (var person in people)
            {
                var results = new List<ValidationResult>();
                var context = new ValidationContext(person);
                if (!Validator.TryValidateObject(person, context, results, true))
                {
                    _logs += person.Name + ", ";
                    foreach (var error in results)
                        Notes(error, person);
                }
                else
                    CleanNotes(person);
                values.Add(results.Count == 0);
            }
            if(_logs!=null)
                _logs =  @"Сохранения не удалось! Ошибки в: "+ _logs;
            return values.All(value => value == true);
        }


        /// <summary>
        /// Метод для связывания свойств содержащих ошибки валидации в модели с ValidationResult
        /// </summary>
        /// <param name="error"></param>
        /// <param name="person"></param>
        static void Notes(ValidationResult error, Person person)
        {
            string err = error.MemberNames.FirstOrDefault();

            switch (err)
            {
                case "Name":
                    person.Errors.NameError = error.ErrorMessage;
                    break;
                case "LastName":
                    person.Errors.LastNameError = error.ErrorMessage;
                    break;
                case "SecondName":
                    person.Errors.SecondNameError = error.ErrorMessage;
                    break;
                case "Email":
                    person.Errors.EmailError = error.ErrorMessage;
                    break;
            }
        }

        /// <summary>
        /// Метод для отчистки полей ошибок после удачной проверки валидации
        /// </summary>
        /// <param name="person"></param>
        static void CleanNotes(Person person)
        {
            person.Errors.NameError = null;
            person.Errors.LastNameError = null;
            person.Errors.SecondNameError = null;
            person.Errors.EmailError = null;
        }
    }
}
