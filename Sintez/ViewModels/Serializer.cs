﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Windows;

namespace Sintez
{
    interface ISerializer
    {
        void Serialize(IEnumerable<Person> people, string path);
        IEnumerable<Person> Deserialize(string path);
    }


    class XmlSerializer : ISerializer
    {
        public IEnumerable<Person> Deserialize(string path)
        {
            ObservableCollection<Person> people = new ObservableCollection<Person>();
            XDocument xdoc = XDocument.Load(path);

            var items = from el in xdoc.Element("People").Elements("Person")
                        select new Person
                        {
                            Name = el.Element("Name").Value,
                            LastName = el.Element("LastName").Value,
                            SecondName = el.Element("SecondName").Value,
                            Email = el.Element("Email").Value
                        };

            foreach (var el in items)
            {
                people.Add(
                    new Person
                    {
                        Name = el.Name,
                        LastName = el.LastName,
                        SecondName = el.SecondName,
                        Email = el.Email
                    });
            }
            
            return people;
        }

        public void Serialize(IEnumerable<Person> people, string path)
        {
            XDocument xdoc = new XDocument();
            XElement root = new XElement("People");
            foreach (var el in people)
            {
                root.Add( new XElement("Person",
                    new XElement("Name", el.Name),
                    new XElement("LastName", el.LastName),
                    new XElement("SecondName", el.SecondName),
                    new XElement("Email", el.Email)));
               
            }
            xdoc.Add(root);
            xdoc.Save(path);
        }
        
    }

    class BinarySerializer : ISerializer
    {
        public IEnumerable<Person> Deserialize(string path)
        {
            ObservableCollection<Person> people = new ObservableCollection<Person>();
            using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                while (reader.PeekChar() > -1)
                {
                    string name = reader.ReadString();
                    string lastName = reader.ReadString();
                    string secondName = reader.ReadString();
                    string email = reader.ReadString();

                    people.Add(new Person { Name = name, LastName = lastName, SecondName = secondName, Email = email });
                }
            }

            return people;
        }

        public void Serialize(IEnumerable<Person> people, string path)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.OpenOrCreate)))
            {
                foreach (var p in people)
                {
                    writer.Write(p.Name);
                    writer.Write(p.LastName);
                    writer.Write(p.SecondName);
                    writer.Write(p.Email);
                }
            }
        }
    }
}
